;Program dengan bahasa assembly menggunakan emu8086
;mengubah sebuah huruf kapital menjadi huruf kecil
;Loki Lang
NAME "CONVERT2"
ORG 100H
START:
MOV AH, 9
MOV DX, PESAN1
INT 21H
MOV AH, 1
INT 21H

CMP AL, 5AH
JG SALAH
CMP AL, 40H
JL SALAH
JGE BESAR

BESAR:
ADD AL, 20H
PUSH AX
MOV AH, 9
MOV DX, PESAN2
INT 21H
POP DX
MOV AH, 2
INT 21H
JMP KELUAR

SALAH:
MOV AH, 9
MOV DX, PESAN3
INT 21H
JMP KELUAR

KELUAR:
MOV AH, 9
MOV DX, PESAN4
INT 21H
MOV AH, 4CH
INT 21H

PESAN1:    db 13,10,'Masukkan sebuah huruf besar.',13,10,'$'
PESAN2:    db 13,10,'Huruf besar tersebut diubah menjadi: ',13,10,'$'
PESAN3:    db 13,10,'Kesalahan, yang Anda masukkan bukan huruf besar.',13,10,'$'
PESAN4:    db 13,10,'Program selesai, kembali ke sistem operasi.',13,10,'$'
END